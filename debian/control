Source: mail-spf-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Scott Kitterman <scott@kitterman.com>
Section: mail
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: liberror-perl <!nocheck>,
                     libnet-dns-perl <!nocheck>,
                     libnet-dns-resolver-programmable-perl <!nocheck>,
                     libnetaddr-ip-perl <!nocheck>,
                     libtest-pod-perl <!nocheck>,
                     liburi-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/mail-spf-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/mail-spf-perl.git
Homepage: https://metacpan.org/release/Mail-SPF
Rules-Requires-Root: no

Package: libmail-spf-perl
Architecture: all
Section: perl
Depends: ${misc:Depends},
         ${perl:Depends},
         liberror-perl,
         libnet-dns-perl,
         libnetaddr-ip-perl,
         liburi-perl
Description: Perl implementation of Sender Policy Framework and Sender ID
 Mail::SPF is an object-oriented Perl implementation of the Sender Policy
 Framework (SPF) e-mail sender authentication system <http://www.openspf.org>.
 .
 It supports both the TXT and SPF RR types as well as both SPFv1 (v=spf1) and
 Sender ID (spf2.0) records, and it is fully compliant to RFCs 4408 and 4406.
 (It does not however implement the patented PRA address selection algorithm
 described in RFC 4407.)

Package: spf-tools-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libmail-spf-perl (>= ${source:Version})
Description: SPF tools (spfquery, spfd) based on the Mail::SPF Perl module
 A collection of Sender Policy Framework (SPF) tools that are based on the
 fully RFC-conforming Mail::SPF Perl module.  The following tools are included
 in this package:
 .
   * spfquery:  A command-line tool for performing SPF checks.
   * spfd:      A daemon for services that perform SPF checks frequently.
